
#!/usr/bin/python
# -∗- coding: utf-8 -∗-

#import csv
import sqlite3

def main(args):
    conn = sqlite3.connect('lacuisinedepablo1.sqlit3')
    curseur = conn.cursor()
    curseur.execute("""CREATE TABLE  IF NOT EXISTS ingredients (
    id  INTEGER NOT NULL,
    amount TEXT,
    unit  TINYTEXT,
    item  TEXT
    )""")

    curseur.execute("""CREATE TABLE IF NOT EXISTS  recettes (
    id        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    title     TINTTEXT NOT NULL,
    saison    TINYTEXT,
    category  TINYTEXT,
    remarque  TEXT,
    instructions  TEXT,
    image     BLOB
    )""")

#    with open('nationalparks.csv', 'r') as f:
#        reader = csv.reader(f.readlines()[1:])  # exclude header line
#        curseur.executemany("""INSERT INTO natlpark VALUES (?,?,?,?)""",
#                        (row for row in reader))
    conn.commit()
    conn.close()

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))

