
#
# Structure de la table `ingredients`
#

CREATE TABLE  IF NOT EXISTS ingredients (
    id  INTEGER NOT NULL,
    amount int(20),
    unit  varchar(20),
    item  TINYTEXT
    )


#
# Structure de la table `recettes`
#

CREATE TABLE IF NOT EXISTS  recettes (
    id        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    title     varchar(200)  NOT NULL,
    saison    varchar(10),
    category  varchar(50) ,
    remarque  TINYTEXT,
    instructions  TEXT,
    image     BLOB
    )

