#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

import sqlite3
import csv

def connect_to_database(db_file):
    try:
        conn = sqlite3.connect(db_file)
		#On active les foreign keys
        conn.execute("PRAGMA foreign_keys = 1")
        return conn
    except Error as e:
        print(e)

    return None


def majBD(conn,cursor,file):
    # Lecture du fichier de script sql et placement des requetes dans un tableau
    createFile = open(file, 'r')
    createSql = createFile.read()
    createFile.close()
    sqlQueries = createSql.split(";")

    # Execution des requetes de création des tables
    for query in sqlQueries:
        try:
            cursor.execute(query)
        except:
            print ("xxxxxxbugxxxxxxxx",query)
    # Effectuer les changements dans la base
    conn.commit()

     # Lecture du fichier de donnees recettes.csv
    with open('recettes.csv',encoding="utf8",newline='') as f:
        dic_recettes = csv.DictReader(f,delimiter='\t',quotechar='"')
        value_recettes = [(each_row['id'], each_row['title'], each_row['saison'],each_row['category'], each_row['remarque'], each_row['instructions'],each_row['image']) for each_row in dic_recettes]
     # multi Insert des enregistrements du fichier
        cursor.executemany("INSERT INTO recettes (id, title,saison, category, remarque, instructions, image) VALUES (?, ?, ?, ?, ?, ?, ?);", value_recettes)

    # Effectuer les changements dans la base
    conn.commit()


    # Lecture du fichier de donnees aliments_db.csv
    with open('aliments_db.csv',encoding="utf8",newline='') as f:
        dic_aliments = csv.DictReader(f,delimiter=';',quotechar='"')
        value_aliments = [(each_row['unite'], each_row['nomA'], each_row['rayon']) for each_row in dic_aliments]
     # multi Insert des enregistrements du fichier
        cursor.executemany("INSERT INTO aliments (unite, nomA,rayon) VALUES (?, ?, ?);", value_aliments)

    # Effectuer les changements dans la base
    conn.commit()

    # Lecture du fichier de donnees ingredients_db.csv
    with open('ingredients_db.csv',encoding="utf8",newline='') as f:
        dic_ingredients = csv.DictReader(f,delimiter=';',quotechar='"')
        value_ingredients = [(each_row['idRecette'], each_row['amount'], each_row['item']) for each_row in dic_ingredients]
     # multi Insert des enregistrements du fichier
        cursor.executemany("INSERT INTO ingredients (idRecette, amount, item) VALUES (?, ?, ?);", value_ingredients)

    # Effectuer les changements dans la base
    conn.commit()

def main(args):
    '''
    Pour initialiser en console:
    $ python3  __init__.py
    '''
    print(args)

    DATABASE = 'lacuisinedepablo.db'
    conn=connect_to_database(DATABASE)
    majBD(conn,conn.cursor(),'lacuisinedepablo_db.sql')
        # fermeture de la connexion
    conn.close()


if __name__ == '__main__':
    import sys
    # sys.argv est la liste des arguments donnés au fichier  __init__.py
    #sys.argv[0] est le nom du fichier, sys.argv[1]  le nom du fichier csv  et que l'on doit indiquer 
    sys.exit(main(sys.argv))

