#!/usr/bin/python3.4
# -*- coding: utf-8 -*-
from flask import Flask, render_template, url_for, request, g
from flask_menu import Menu, register_menu
import sqlite3

from flaskext.markdown import Markdown


#majBD(conn.cursor(),'lacuisinedepablo_db.sql') #a commenter/décommenter avec DATABASE='lacuisinedepablo.db'


app = Flask(__name__) # Initialise l'application Flask
# J'ai ajouter des extensions pour disposer du redimensionnement des images markdown syntaxe:  ![nom](url_image){: width=100px}
#Markdown(app, extensions=['codehilite', 'fenced_code', 'tables', 'attr_list'])

Markdown(app, extensions=['tables', 'attr_list'])
Menu(app=app)
DATABASE = 'lacuisinedepablo.sqlite3'


#doc:   https://flask.palletsprojects.com/en/1.1.x/patterns/sqlite3/
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, 'lacuisinedepablo', None)
    if db is not None:
        db.close()


@app.route('/', methods=['GET','POST'])
@register_menu(app, '.home', 'Home',order=1)
def accueil():
    c = get_db().cursor()


    listecategorie=c.execute("SELECT  DISTINCT  category  FROM recettes ORDER BY category").fetchall()  # On obtient la liste des catégorie
    listecategorie=[i[0] for i in listecategorie]  # On enlève les tupples pour ne garder que la liste % .capitalize() et mettre la 1° lettre en majuscule
    if listecategorie[0]!='':
        listecategorie=['']+listecategorie

    if request.method == 'GET':
        req=""
        nomrecette=''
        ingredient=''
        category=''
        id=''
    else:
        nomrecette=request.form['nomrecette']
        ingredient=request.form['ingredient']
        category=request.form['category']
        valnomreq="" #"SELECT * FROM recette WHERE  "

        valnomreq=f"SELECT  DISTINCT recettes.id,recettes.title FROM recettes,ingredients  WHERE recettes.title  LIKE '%{nomrecette}%'  AND  ingredients.id=recettes.id   AND  ingredients.item LIKE '%{ingredient}%'"  #.format(nomrecette,ingredient)
        if request.form['category']!='':
            valnomreq+=f" AND category='{category}'"  #.format(category)
        valnomreq+=" ORDER BY recettes.title"
        try: req=c.execute(valnomreq).fetchall()
        except: req=[]
        #c.execute("SELECT  DISTINCT title FROM recettes,ingredients  WHERE recettes.title  LIKE '%noix%'  AND  ingredients.id=recettes.id   AND  ingredients.item LIKE '%farine%' AND category='Dessert'").fetchall()
    return render_template("accueil.html", titre="Bienvenue !",req=req,nomrecette=nomrecette,  ingredient=ingredient,  category=category,listecategorie=listecategorie)


@app.route('/recette/<int:recette_id>')
@app.route('/recette')
@app.route('/recette/')
@register_menu(app, '.recette', 'Recette', order=2)
def fonction_recette(recette_id=''):
    c = get_db().cursor()
    recette=c.execute(f"SELECT  title,category,instructions,image FROM recettes  WHERE id='{recette_id}'").fetchall()  # On a une liste d'un tupple de la recette
    try: recette=recette[0]  # On ne garde que le tuple
    except: recette=[]

    ingredient=c.execute(f"SELECT  amount,unit,item FROM ingredients WHERE id='{recette_id}'").fetchall() # On a une liste de tuple des ingrédients
    try: titre=recette[0]  # Le 1° élément est le titre
    except: titre="Recette"
    return render_template("recette.html", titre=titre,recette=recette,ingredient=ingredient)



#@app.route('/recette/<id>')
#@register_menu(app, '.recette', 'Recette', order=2)
#def recette(id):
#    return render_template("recette.html", titre="Recette")


@app.route('/informations')
@register_menu(app, '.informations', 'Informations', order=3)
def information():
    return render_template("informations.html", titre="Informations")

@app.route('/admin')
@register_menu(app, '.administration', 'Administration', order=4)
def administration():
    return render_template("administration.html", titre="administration")

if __name__ == '__main__':
    app.run(debug=True)
