DROP TABLE IF EXISTS aliments;
DROP TABLE IF EXISTS recettes;
DROP TABLE IF EXISTS ingredients;


CREATE TABLE  IF NOT EXISTS  aliments (
    nomA TINYTEXT,
    unite varchar(20),
    rayon varchar(20),
  CONSTRAINT pk_aliments PRIMARY KEY (nomA)
    );





CREATE TABLE  IF NOT EXISTS recettes (
    id        INTEGER NOT NULL,
    title     varchar(200)  NOT NULL,
    saison    varchar(10),
    category  varchar(50) ,
    remarque  TINYTEXT,
    instructions  TEXT,
    image     BLOB,
  CONSTRAINT pk_recettes PRIMARY KEY (id)
    );

CREATE TABLE  IF NOT EXISTS ingredients (
    idRecette  INTEGER NOT NULL,
    amount varchar(20),
    item  TINYTEXT,
  CONSTRAINT pk_ingredients PRIMARY KEY (idRecette, item),
  FOREIGN KEY (idRecette) REFERENCES recettes(id),
  FOREIGN KEY (item) REFERENCES aliments(nomA)
    );


